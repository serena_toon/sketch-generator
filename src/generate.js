import sketch from 'sketch'
import { Names } from './Names.js'

const NUM_OF_NAMES = Names.firstNames.length; // used for random number generation limit

/**
 * Main function, runs on execution of the command from Sketch
 */
export default function() {
  const selectedLayers = context.selection; // get the selected layers
  const selectedCount = selectedLayers.length; // number of selected layers

  if (selectedCount == 0) { // if no elements in selection
    sketch.UI.message('No layers selected!');
    return; // do nothing if no layers selected
  } else if (isValidLayers(selectedLayers) == false) {
    sketch.UI.message('No shape or text layers selected!');
    return;
  } else {
    if (isMatchedPair(selectedLayers)) {
      generateMatchedPair(selectedLayers);
    } else {
      generateContent(selectedLayers);
    }
  }
  return;
}

/**
 * Determines whether or not the input layers are a 'matched pair'
 * i.e. ONE layer is a TEXT layer and ONE layer is a SHAPE layer
 * 
 * @param {[]} layers Array of selected layers
 * @return {boolean} Whether or not the input layers are a matched pair
 */
function isMatchedPair(layers) {
  if (layers.length !== 2) { // a matched pair can only consist of exactly 2 layers
    return false;
  } else if (isShapeLayer(layers[0]) && isTextLayer(layers[1]) ||
      isTextLayer(layers[0]) && isShapeLayer(layers[1])) {
    // checking that the array is made up of exactly one text layer and one shape layer
    return true;
  } else {
    return false;
  }
}

/**
 * Generates and replaces content for the selected layers
 * 
 * @param {[]} layers Array of selected layers
 */
function generateContent(layers) {
  layers.forEach(layer => {
    if (isShapeLayer(layer)) {
      replaceShape(layer);
    } else if (isTextLayer(layer)) {
      replaceText(layer);
    }
  })
}

/**
 * Determines whether or not the input layer is of type Shape
 * 
 * @param {*} layer Sketch layer
 * @return {boolean} Whether or not the input layer is of type Shape
 */
function isShapeLayer(layer) {
  // layer type can be determined by its class
  const layerClass = layer.class().toString();
  const isPath = layerClass.includes('Path'); // paths are called MSShapePathLayer
  if (isPath) {
    return false;
  } else {
    return layerClass.includes('Shape'); // e.g. MSOvalShape, MSRectangleShape
  }
}

/**
 * Determines whether or not the input layer is of type Text
 * 
 * @param {*} layer Sketch layer
 * @return {boolean} Whether or not the input layer is of type Text
 */
function isTextLayer(layer) {
  // layer type can be determined by its class
  const layerClass = layer.class().toString();
  return layerClass.includes('Text'); // e.g. MSTextLayer
}

/**
 * Determines whether or nor the input layers are valid layers
 * i.e. are shape and/or text layers
 * 
 * @param {[]} layers Array of selected layers
 * @return {boolean} Boolean indicating whether or not input layers are valid
 */
function isValidLayers(layers) {
  for (let i = 0; i < layers.length; i++) {
    if (isShapeLayer(layers[i]) || isTextLayer(layers[i])) {
      return true; // as soon a valid layer is found, plugin will proceed
    }
  }
  return false; // if it reaches end of array without finding a shape or text layer, plugin does not do anything. should alert
}

/**
 * Replaces the input layer's image fill with an image
 * If 'name' parameter is not specified, it will replace with a random image
 * If 'name' parameter is specified, it will replace with image of that filename, suffixed with '.jpg'
 * 
 * @param {*} layer Shape layer
 * @param {String} name Name of the image it should be replaced with, used for matched pairs.  Can be null if not a matched pair.
 */
function replaceShape(layer, name) {
  const fill = layer.style().fills().firstObject(); // get the fill object
  fill.setFillType(4); // set fill type as image
  let nsImage = null;
  nsImage = getImage(name); // this will either return a random image or a 'matched' image dictated by name parameter
  const img = MSImageData.alloc().initWithImage(nsImage); // get image data
  fill.setImage(img);
  fill.setPatternFillType(1); // set fill display type to Fill
}

/**
 * Replaces the input text layer's text
 * If 'name' parameter is not specified, it will replace with a random name
 * If 'name' parameter is specified, it will replace with that string
 * Also replaces the label of the text layer, as well as resizing the textbox
 * 
 * @param {*} layer Text layer
 * @param {String} name String that the layer text should be replaced with, used for matched pairs.  Can be null if not a matched pair.
 */
function replaceText(layer, name) {
  let newText = '';
  if (!name) { 
    newText = getRandomName(NUM_OF_NAMES); // if name param not specified, get random name
  } else {
    newText = name; // matched pair
  }
  layer.setStringValue(newText); // set new text
  layer.setName(newText); // set layer name to the new string
  layer.adjustFrameToFit(); // resize textbox
}

/**
 * Generates a matched pair
 * i.e. a matching image and name
 * 
 * @param {[]} layers Array of layers of size 2, containing a matched pair
 */
function generateMatchedPair(layers) {
  const name = getRandomMatchedName(NUM_OF_NAMES);
  if (isShapeLayer(layers[0])) { // if first element in array is a Shape layer
    replaceShape(layers[0], name);
    replaceText(layers[1], name); // the second element must be a Text layer, given that it is a matched pair
  } else {
    replaceShape(layers[1], name);
    replaceText(layers[0], name);
  }
}

/**
 * Retrieves either an image with filename 'name' sans extension, or a random image.
 * 
 * @param {String} name Name of the image to be fetched, without the file extension.  Can be null if not a matched pair.
 * @return {NSImage} NSImage data of the image retrieved
 */
function getImage(name) {
  const fileManager = NSFileManager.defaultManager();
  let folder = context.plugin.url() + 'Contents/Resources/images/';
  folder = folder.replace('%20', ' ').replace('file://', ''); // directory where the resources are stored

  const files = fileManager.contentsOfDirectoryAtPath_error(folder, null); // array of all the files in the folder
  let filePath = '';
  if (name) { // if matched pair
    filePath = folder + name + '.jpg';
  } else { // otherwise, get random image
    filePath = folder + files[getRandomNumber(files.count())];
  }

  return NSImage.alloc().initWithContentsOfFile(filePath);
}

/**
 * Gets a random integer, with max value 'limit'
 * 
 * @param {Number} limit Maximum number that should be returned, i.e. the upper limit of the number range to get a random number from
 * @return {Number} Random number from range 0-limit
 */
function getRandomNumber(limit) {
  return Math.floor(Math.random() * limit);
}

/**
 * Gets a random name, i.e. random first name and random last name combination
 * 
 * @param {Number} max 
 * @return {String} A full name (i.e. firstname and lastname) with a space inbetween
 */
function getRandomName(max) {
  const firstName = Names.firstNames[getRandomNumber(max)];
  const lastName = Names.lastNames[getRandomNumber(max)];
  return firstName + ' ' + lastName; // concatenate firstname and lastname with a space in between
}

/**
 * Gets a random MATCHED name, i.e. a matched firstname and lastname combination
 * To be used in the generation of a matched pair name/avatar
 * 
 * @param {Number} max 
 * @return {String} A full name (i.e. firstname and lastname) with a space inbetween
 */
function getRandomMatchedName(max) {
  // matched name means that the names come from the same index of the firstname and lastname array
  const index = getRandomNumber(max);
  return Names.firstNames[index] + ' ' + Names.lastNames[index];
}