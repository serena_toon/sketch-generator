const firstNames = [
    'Harley', 'Marcus', 'Harrison', 'Troy', 'Zane', 'Joshua', 'Aaron', 'Adam', 'Michael',
    'Leo', 'Rebecca', 'Shannon', 'Priscilla', 'Mikaela', 'Ava', 'Lexie', 'Carly', 'Vivian',
    'Heidi', 'April', 'Bruno', 'Tom', 'Elon', 'Audrey', 'Katy', 'Olivia', 'Demi', 'John',
    'Frank', 'Natalie', 'Dwayne', 'Cristiano', 'Chris', 'Godfrey'
];

const lastNames = [
    'Grant', 'Williams', 'Jones', 'Griffith', 'Henderson', 'Reed', 'Fischer', 'Cook', 'May',
    'Levine', 'Pickett', 'Butler', 'Burke', 'Harper', 'Dillard', 'Wheeler', 'Haynes',
    'Watts', 'Smith', 'Robertson', 'Mars', 'Hanks', 'Musk', 'Hepburn', 'Perry', 'Wilde',
    'Moore', 'Legend', 'Ocean', 'Portman', 'Johnson', 'Ronaldo', 'Hemsworth', 'Gao'
];

// put the two arrays into one object for easier exporting
const Names = {};
Names.firstNames = firstNames;
Names.lastNames = lastNames;
export { Names };