# Generator Sketch Plugin

Generate dummy content including avatars, names, and matched avatar-name pairs.

## Installation

Double-click on sketch-generate.sketchplugin to install the plugin.

## Usage

To run the plugin, select some layers and navigate to the `Plugins` menu in Sketch, and under `Generator`, click on `Generate content`.

There are several different combinations of selected layers which will result in different results:

### Text layer(s) selected
To generate random names, select text layers and run the plugin.  This will replace the text with a random firstname and lastname.

### Shape layer(s) selected
To generate random avatars, select shape layers and run the plugin.  This will replace the image fill with a random image.

### Multiple text layers and shape layers selected
Having more than one text layer and/or more than one shape layer selected together will result in random names and avatars being generated.

### **ONE** text layer ***AND*** **ONE** shape layer selected
Selecting a single shape layer AND a single text layer and running the plugin will cause a random 'matched pair' to be generated.  That is, a matched full name and avatar pairing.
This will only happen if ONE single shape layer and ONE single text layer are selected together.
Matched pairs are made up of a collection of made-up names as well as a few famous people.

## Development

### Building

The plugin can be built from source with the command `skpm run build` while in the root directory.

To continuously listen to changes and build accordingly, execute `skpm run watch` instead.

### File structure

#### Resources

Images are stored in `res/images`.

#### Source files

Source files are found under `src/`.

##### `generate.js`

This is the main script file.  This contains the main function that is run when the `Generate content` command is executed, alongside its helper functions.

##### `names.js`

This is where the arrays of names are stored.  It is made up of two arrays, a `firstName` and `lastName` array.