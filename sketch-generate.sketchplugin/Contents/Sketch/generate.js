var that = this;
function __skpm_run (key, context) {
  that.context = context;

var exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/generate.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Names.js":
/*!**********************!*\
  !*** ./src/Names.js ***!
  \**********************/
/*! exports provided: Names */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Names", function() { return Names; });
var firstNames = ['Harley', 'Marcus', 'Harrison', 'Troy', 'Zane', 'Joshua', 'Aaron', 'Adam', 'Michael', 'Leo', 'Rebecca', 'Shannon', 'Priscilla', 'Mikaela', 'Ava', 'Lexie', 'Carly', 'Vivian', 'Heidi', 'April', 'Bruno', 'Tom', 'Elon', 'Audrey', 'Katy', 'Olivia', 'Demi', 'John', 'Frank', 'Natalie', 'Dwayne', 'Cristiano', 'Chris', 'Godfrey'];
var lastNames = ['Grant', 'Williams', 'Jones', 'Griffith', 'Henderson', 'Reed', 'Fischer', 'Cook', 'May', 'Levine', 'Pickett', 'Butler', 'Burke', 'Harper', 'Dillard', 'Wheeler', 'Haynes', 'Watts', 'Smith', 'Robertson', 'Mars', 'Hanks', 'Musk', 'Hepburn', 'Perry', 'Wilde', 'Moore', 'Legend', 'Ocean', 'Portman', 'Johnson', 'Ronaldo', 'Hemsworth', 'Gao']; // put the two arrays into one object for easier exporting

var Names = {};
Names.firstNames = firstNames;
Names.lastNames = lastNames;


/***/ }),

/***/ "./src/generate.js":
/*!*************************!*\
  !*** ./src/generate.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sketch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sketch */ "sketch");
/* harmony import */ var sketch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sketch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Names_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Names.js */ "./src/Names.js");


var NUM_OF_NAMES = _Names_js__WEBPACK_IMPORTED_MODULE_1__["Names"].firstNames.length; // used for random number generation limit

/**
 * Main function, runs on execution of the command from Sketch
 */

/* harmony default export */ __webpack_exports__["default"] = (function () {
  var selectedLayers = context.selection; // get the selected layers

  var selectedCount = selectedLayers.length; // number of selected layers

  if (selectedCount == 0) {
    // if no elements in selection
    sketch__WEBPACK_IMPORTED_MODULE_0___default.a.UI.message('No layers selected!');
    return; // do nothing if no layers selected
  } else if (isValidLayers(selectedLayers) == false) {
    sketch__WEBPACK_IMPORTED_MODULE_0___default.a.UI.message('No shape or text layers selected!');
    return;
  } else {
    if (isMatchedPair(selectedLayers)) {
      generateMatchedPair(selectedLayers);
    } else {
      generateContent(selectedLayers);
    }
  }

  return;
});
/**
 * Determines whether or not the input layers are a 'matched pair'
 * i.e. ONE layer is a TEXT layer and ONE layer is a SHAPE layer
 * 
 * @param {[]} layers Array of selected layers
 * @return {boolean} Whether or not the input layers are a matched pair
 */

function isMatchedPair(layers) {
  if (layers.length !== 2) {
    // a matched pair can only consist of exactly 2 layers
    return false;
  } else if (isShapeLayer(layers[0]) && isTextLayer(layers[1]) || isTextLayer(layers[0]) && isShapeLayer(layers[1])) {
    // checking that the array is made up of exactly one text layer and one shape layer
    return true;
  } else {
    return false;
  }
}
/**
 * Generates and replaces content for the selected layers
 * 
 * @param {[]} layers Array of selected layers
 */


function generateContent(layers) {
  layers.forEach(function (layer) {
    if (isShapeLayer(layer)) {
      replaceShape(layer);
    } else if (isTextLayer(layer)) {
      replaceText(layer);
    }
  });
}
/**
 * Determines whether or not the input layer is of type Shape
 * 
 * @param {*} layer Sketch layer
 * @return {boolean} Whether or not the input layer is of type Shape
 */


function isShapeLayer(layer) {
  // layer type can be determined by its class
  var layerClass = layer.class().toString();
  var isPath = layerClass.includes('Path'); // paths are called MSShapePathLayer

  if (isPath) {
    return false;
  } else {
    return layerClass.includes('Shape'); // e.g. MSOvalShape, MSRectangleShape
  }
}
/**
 * Determines whether or not the input layer is of type Text
 * 
 * @param {*} layer Sketch layer
 * @return {boolean} Whether or not the input layer is of type Text
 */


function isTextLayer(layer) {
  // layer type can be determined by its class
  var layerClass = layer.class().toString();
  return layerClass.includes('Text'); // e.g. MSTextLayer
}
/**
 * Determines whether or nor the input layers are valid layers
 * i.e. are shape and/or text layers
 * 
 * @param {[]} layers Array of selected layers
 * @return {boolean} Boolean indicating whether or not input layers are valid
 */


function isValidLayers(layers) {
  for (var i = 0; i < layers.length; i++) {
    if (isShapeLayer(layers[i]) || isTextLayer(layers[i])) {
      return true; // as soon a valid layer is found, plugin will proceed
    }
  }

  return false; // if it reaches end of array without finding a shape or text layer, plugin does not do anything. should alert
}
/**
 * Replaces the input layer's image fill with an image
 * If 'name' parameter is not specified, it will replace with a random image
 * If 'name' parameter is specified, it will replace with image of that filename, suffixed with '.jpg'
 * 
 * @param {*} layer Shape layer
 * @param {String} name Name of the image it should be replaced with, used for matched pairs.  Can be null if not a matched pair.
 */


function replaceShape(layer, name) {
  var fill = layer.style().fills().firstObject(); // get the fill object

  fill.setFillType(4); // set fill type as image

  var nsImage = null;
  nsImage = getImage(name); // this will either return a random image or a 'matched' image dictated by name parameter

  var img = MSImageData.alloc().initWithImage(nsImage); // get image data

  fill.setImage(img);
  fill.setPatternFillType(1); // set fill display type to Fill
}
/**
 * Replaces the input text layer's text
 * If 'name' parameter is not specified, it will replace with a random name
 * If 'name' parameter is specified, it will replace with that string
 * Also replaces the label of the text layer, as well as resizing the textbox
 * 
 * @param {*} layer Text layer
 * @param {String} name String that the layer text should be replaced with, used for matched pairs.  Can be null if not a matched pair.
 */


function replaceText(layer, name) {
  var newText = '';

  if (!name) {
    newText = getRandomName(NUM_OF_NAMES); // if name param not specified, get random name
  } else {
    newText = name; // matched pair
  }

  layer.setStringValue(newText); // set new text

  layer.setName(newText); // set layer name to the new string

  layer.adjustFrameToFit(); // resize textbox
}
/**
 * Generates a matched pair
 * i.e. a matching image and name
 * 
 * @param {[]} layers Array of layers of size 2, containing a matched pair
 */


function generateMatchedPair(layers) {
  var name = getRandomMatchedName(NUM_OF_NAMES);

  if (isShapeLayer(layers[0])) {
    // if first element in array is a Shape layer
    replaceShape(layers[0], name);
    replaceText(layers[1], name); // the second element must be a Text layer, given that it is a matched pair
  } else {
    replaceShape(layers[1], name);
    replaceText(layers[0], name);
  }
}
/**
 * Retrieves either an image with filename 'name' sans extension, or a random image.
 * 
 * @param {String} name Name of the image to be fetched, without the file extension.  Can be null if not a matched pair.
 * @return {NSImage} NSImage data of the image retrieved
 */


function getImage(name) {
  var fileManager = NSFileManager.defaultManager();
  var folder = context.plugin.url() + 'Contents/Resources/images/';
  folder = folder.replace('%20', ' ').replace('file://', ''); // directory where the resources are stored

  var files = fileManager.contentsOfDirectoryAtPath_error(folder, null); // array of all the files in the folder

  var filePath = '';

  if (name) {
    // if matched pair
    filePath = folder + name + '.jpg';
  } else {
    // otherwise, get random image
    filePath = folder + files[getRandomNumber(files.count())];
  }

  return NSImage.alloc().initWithContentsOfFile(filePath);
}
/**
 * Gets a random integer, with max value 'limit'
 * 
 * @param {Number} limit Maximum number that should be returned, i.e. the upper limit of the number range to get a random number from
 * @return {Number} Random number from range 0-limit
 */


function getRandomNumber(limit) {
  return Math.floor(Math.random() * limit);
}
/**
 * Gets a random name, i.e. random first name and random last name combination
 * 
 * @param {Number} max 
 * @return {String} A full name (i.e. firstname and lastname) with a space inbetween
 */


function getRandomName(max) {
  var firstName = _Names_js__WEBPACK_IMPORTED_MODULE_1__["Names"].firstNames[getRandomNumber(max)];
  var lastName = _Names_js__WEBPACK_IMPORTED_MODULE_1__["Names"].lastNames[getRandomNumber(max)];
  return firstName + ' ' + lastName; // concatenate firstname and lastname with a space in between
}
/**
 * Gets a random MATCHED name, i.e. a matched firstname and lastname combination
 * To be used in the generation of a matched pair name/avatar
 * 
 * @param {Number} max 
 * @return {String} A full name (i.e. firstname and lastname) with a space inbetween
 */


function getRandomMatchedName(max) {
  // matched name means that the names come from the same index of the firstname and lastname array
  var index = getRandomNumber(max);
  return _Names_js__WEBPACK_IMPORTED_MODULE_1__["Names"].firstNames[index] + ' ' + _Names_js__WEBPACK_IMPORTED_MODULE_1__["Names"].lastNames[index];
}

/***/ }),

/***/ "sketch":
/*!*************************!*\
  !*** external "sketch" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sketch");

/***/ })

/******/ });
  if (key === 'default' && typeof exports === 'function') {
    exports(context);
  } else {
    exports[key](context);
  }
}
that['onRun'] = __skpm_run.bind(this, 'default')

//# sourceMappingURL=generate.js.map